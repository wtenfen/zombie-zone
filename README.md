# README

## Requirements 
* Ruby 2.5.1
* bundler
* Postgresql 9.4+
## RVM
 * Install ruby-version-manager for easy setup. 
 * This project already has .ruby-version and .ruby-gemset files for working with RVM
## Setup Project
 Setup database
 
  `setup user/password/host at config/database.yaml`
 
 Install dependencies:
 
  `cd to project root path`
  
  `run: bin/setup`
  
  To load sample data:
  
  `run: rake db:seed`
  
## SPECS
To run spec

`cd to project root path`

`run: rspec rspec/`

## COVERAGE

Coverage is used for code coverage. When running specs coverage files are generated on /coverage

## API DOCS

API docs are created by acceptance specs. Docs files will be generated at /doc/api
  
`cd do project root path`

`run: rake docs:generate`
  
 
