class ReportedInfectionJob < ApplicationJob
  queue_as :default

  def perform(reporter, infected)
    UpdateToInfected.new(reporter, infected).call
  end
end
