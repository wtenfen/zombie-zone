# == Schema Information
#
# Table name: survivor_items
#
#  id          :bigint(8)        not null, primary key
#  survivor_id :bigint(8)
#  item_id     :bigint(8)
#  total       :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class SurvivorItem < ApplicationRecord
  belongs_to :survivor
  belongs_to :item

  validates_presence_of :survivor, :item
  validates_uniqueness_of :item, scope: :survivor

  delegate :title, to: :item, allow_nil: true

  class << self
    alias_method :table, :arel_table
  end
end
