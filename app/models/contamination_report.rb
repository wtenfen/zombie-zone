class ContaminationReport < ApplicationRecord
  belongs_to :reporter, class_name: :'Survivor', foreign_key: :reporter_id
  belongs_to :infected, class_name: :'Survivor', foreign_key: :infected_id

  validates_uniqueness_of :reporter, scope: :infected
end
