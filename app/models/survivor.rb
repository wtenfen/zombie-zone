# == Schema Information
#
# Table name: survivors
#
#  id         :bigint(8)        not null, primary key
#  name       :string
#  age        :integer
#  gender     :string(25)
#  lat        :float
#  long       :float
#  status     :string(25)       default("active")
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Survivor < ApplicationRecord
  has_many :items, class_name: 'SurvivorItem'
  enum status: { active: 'active', infected: 'infected'}

  validates_presence_of :name
  validates :lat , numericality: { greater_than_or_equal_to:  -90, less_than_or_equal_to:  90 }
  validates :long, numericality: { greater_than_or_equal_to: -180, less_than_or_equal_to: 180 }

  def can_trade?
    active?
  end

end
