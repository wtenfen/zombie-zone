# == Schema Information
#
# Table name: items
#
#  id         :bigint(8)        not null, primary key
#  title      :string
#  points     :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Item < ApplicationRecord
end
