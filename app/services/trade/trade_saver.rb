module Trade
  class TradeSaver
    include ActiveModel::Model
    attr_reader :first_trade, :second_trade

    validate :validate_trades

    def initialize(first_trade, second_trade)
      @first_trade = first_trade
      @second_trade = second_trade
      @first_survivor = first_trade.trader
      @second_survivor = second_trade.trader
    end

    def call
      return false unless valid?
      do_trade
      true
    end

    def validate_trades
      has_enough_items(first_trade)
      has_enough_items(second_trade)
    end

    def has_enough_items(trade)
      trade.items.each do |item|
        survivor_item = trade.trader.items.find_by(item: item.item.id)
        if survivor_item == nil || survivor_item.total < item.amount
          errors.add(:base, "Survivor: #{trade.trader.id} does not have enough of item: #{item.item.title}")
        end
      end
    end

    def do_trade
      SurvivorItem.transaction do
        exchange_items(first_trade, first_trade.trader, second_trade.trader)
        exchange_items(second_trade, second_trade.trader, first_trade.trader)
      end
    end

    def exchange_items(trade, from, to)
      trade.items.each do |trade_item|
        from_item = from.items.find_by(item: trade_item.item)
        from_item.total -= trade_item.amount
        to_item = to.items.find_or_initialize_by(item: trade_item.item)
        to_item.total += trade_item.amount
        from_item.save!
        to_item.save!
      end
    end

  end
end
