module Trade
  class SimplePointsCalculator
    def initialize(items: [])
      @items = items
    end

    def total_points
      @total_points ||= calculate
    end

    private

    def calculate
      @items.reduce(0) {|sum, item| sum + (item.item.points * item.amount) }
    end
  end
end
