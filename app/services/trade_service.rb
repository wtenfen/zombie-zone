class TradeService
  include ActiveModel::Model

  validate :validate_trade_parties, :validate_total_points

  def initialize(payload)
    @payload = payload
    @first_trader = Survivor.find @payload[:first_trader][:survivor_id]
    @second_trader = Survivor.find @payload[:second_trader][:survivor_id]
  end

  def call
    return false unless valid?
    result = trade_saver.call
    return true if result
    merge_services_errors
    result
  end

  private

  def validate_trade_parties
    if @first_trader == @second_trader
      errors.add(:base, 'More than one survivor should be involved')
    end
    unless @first_trader.can_trade? && @second_trader.can_trade?
      errors.add(:base, 'Cannot trade with infected survivors')
    end
  end

  def validate_total_points
    unless first_trader_points == second_trader_points
      errors.add(:base, 'Both sides of the trade should offer the same amount of points')
    end
  end

  def first_trader_points
    @first_trader_points ||=
      ::Trade::SimplePointsCalculator.new(items: first_items)
        .total_points
  end

  def second_trader_points
    @second_trader_points ||=
      ::Trade::SimplePointsCalculator.new(items: second_items)
        .total_points
  end

  def first_items
    @first_items ||= create_calcutor_items(@payload[:first_trader][:items])
  end

  def second_items
    @second_items ||= create_calcutor_items(@payload[:second_trader][:items])
  end

  def create_calcutor_items(items_payload)
    items_payload.map do |item|
      build_calculator_item(Item.find(item[:item_id]), item[:amount] )
    end
  end

  def build_calculator_item(item, amount)
    OpenStruct.new(item: item, amount: amount)
  end

  def trade_saver
    @trade_saver ||= ::Trade::TradeSaver
      .new(OpenStruct.new(trader: @first_trader, items: first_items), OpenStruct.new(trader: @second_trader, items: second_items))
  end

  def merge_services_errors
    errors.merge!(trade_saver.errors)
  end

end
