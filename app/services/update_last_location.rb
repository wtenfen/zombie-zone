class UpdateLastLocation
  include ActiveModel::Model
  attr_reader :lat, :long

  validates :lat , numericality: { greater_than_or_equal_to:  -90, less_than_or_equal_to:  90 }
  validates :long, numericality: { greater_than_or_equal_to: -180, less_than_or_equal_to: 180 }

  def initialize(survivor, payload)
    @survivor = survivor
    @payload = payload
    @lat = payload[:lat]
    @long = payload[:long]
  end

  def call
    return false unless valid?
    @survivor.lat = lat
    @survivor.long = long
    @survivor.save(validate: false)
  end

end
