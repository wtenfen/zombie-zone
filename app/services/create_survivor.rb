class CreateSurvivor
  attr_reader :survivor, :payload

  delegate :errors, to: :survivor

  def initialize(payload)
    @payload = payload
    @survivor = Survivor.new(payload.except(:items_attributes))
    @items_attributes = payload[:items_attributes]
    @normalized_items = nil
  end

  def call
    set_items if @items_attributes
    @survivor.save
  end

  private

  def set_items
    normalized_items.each do |item|
      item = Item.find_by id: item[:id]
      survivor.items.build(item: item, total: item[:total]) if item
    end
  end

  def normalized_items
    return @normalized_items if @normalized_items
    @normalized_items = NormalizeItemsPayload.new(@items_attributes).build
  end

  class NormalizeItemsPayload
    def initialize(items_payload)
      @items_payload = items_payload
    end

    # sample items_payload: [{id: 1, total: 2},{id: 5, total: nil},{id: 1, total: 2},{id: 5, total: 5}]
    # This method is used to normalize input data from api request
    # It groups by 'id' attribute and sums their 'total' attribute
    def build
      @items_payload.group_by { |x| x[:id] }.map {
        |k, v| {id: k, :total => v.map { |h| h[:total].to_i }.reduce(:+) }
      }.sort_by{ |hash| hash[:id] }
    end
  end

end
