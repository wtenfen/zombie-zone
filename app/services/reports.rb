class Reports
  class << self
    def survivors_by_status
      total = Survivor.count
      active = Survivor.active.count
      infected = Survivor.infected.count
      [
        {
          status: 'active',
          percentage: percentage(total, active)
        },
        {
          status: 'infected',
          percentage: percentage(total, infected)
        }
      ]
    end

    def average_resources
      active_survivors = Survivor.active.count
      total_items = ActiveRecord::Base.connection.execute(average_resources_sql).to_a
      total_items.map{ |el| { average: el["total"].to_f/active_survivors.to_f, item: el["title"]} }
    end

    def total_lost_points
      ActiveRecord::Base.connection.execute(total_lost_points_sql).to_a
    end

    def percentage(total, value)
      return 0 if value.zero?
      (value.to_f * 100.to_f) / total.to_f
    end

    def average_resources_sql
      <<-SQL
        SELECT item_total.total, items.title
        FROM
          (SELECT item_id, SUM(total) as total
             FROM survivor_items
             INNER JOIN survivors on survivor_items.survivor_id = survivors.id
             WHERE survivors.status='#{Survivor.statuses[:active]}'
             GROUP BY item_id
           ) as item_total
        INNER JOIN items on items.id = item_total.item_id
      SQL
    end

    def total_lost_points_sql
      <<-SQL
        SELECT SUM((item_total.total * items.points)) as total_lost_points
        FROM
          (SELECT item_id, SUM(total) as total
            FROM survivor_items
            INNER JOIN survivors on survivor_items.survivor_id = survivors.id
            WHERE survivors.status='#{Survivor.statuses[:infected]}'
            GROUP BY item_id
          ) as item_total
        INNER JOIN items on items.id = item_total.item_id
      SQL
    end
  end
end
