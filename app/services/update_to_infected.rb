class UpdateToInfected
  attr_reader :reporter, :infected

  def initialize(reporter, infected)
    @reporter = reporter
    @infected = infected
  end

  def call
    ContaminationReport.find_or_create_by(reporter: reporter, infected: infected)
    return true if infected.infected?
    infected.infected! if update_to_infected?
  end

  protected

  def update_to_infected?
    count_infection_warnings >= 3
  end

  def count_infection_warnings
    ContaminationReport.where(infected: infected).count
  end

end
