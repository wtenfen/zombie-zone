class MainController < ActionController::API
  rescue_from ActionController::ParameterMissing, with: :missing_param_error
  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found

  def missing_param_error(exception)
    render status: :unprocessable_entity, json: { error: exception.message }
  end

  def record_not_found(exception)
    render status: :bad_request, json: { error: exception.message }
  end

end
