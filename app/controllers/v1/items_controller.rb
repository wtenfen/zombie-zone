class V1::ItemsController < V1::ApplicationController
  def index
    @items = Item.all
    render json: @items
  end
end
