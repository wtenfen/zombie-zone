class V1::TradeItemsController < V1::ApplicationController
  def create
    service = TradeService.new(required_params)
    if service.call
      head :created
    else
      render json: service.errors, status: :bad_request
    end
  end

  protected

  def required_params
    @required_params ||= params.require(:trade)
                          .permit(first_trader: [:survivor_id, items: [:item_id, :amount]],
                                  second_trader: [:survivor_id, items: [:item_id, :amount]])
  end

end
