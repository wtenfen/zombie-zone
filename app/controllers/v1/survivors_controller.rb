class V1::SurvivorsController < V1::ApplicationController
  def create
    service = CreateSurvivor.new(required_params)
    if service.call
      render json: service.survivor, status: :created
    else
      render json: service.errors, status: :bad_request
    end
  end

  protected

  def required_params
    params.require(:survivor).permit(:name, :age, :gender, :lat, :long, items_attributes: [:id, :total])
  end

end
