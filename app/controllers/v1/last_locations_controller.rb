class V1::LastLocationsController < V1::ApplicationController
  def update
    survivor = Survivor.find(required_params[:survivor_id])
    service = UpdateLastLocation.new(survivor, required_params)
    if service.call
      head :ok
    else
      render json: service.errors, status: :bad_request
    end
  end

  def required_params
    @required_params ||= params.require(:location).permit(:survivor_id, :lat, :long)
  end
end
