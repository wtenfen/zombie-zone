class V1::ReportInfectionController < V1::ApplicationController
  def create
    reporter = Survivor.find required_params[:reporter_id]
    infected = Survivor.find required_params[:infected_id]
    ReportedInfectionJob.perform_later(reporter, infected)
    head :ok
  end

  protected

  def required_params
    @required_params ||= params.require(:infection).permit(:reporter_id, :infected_id)
  end

end
