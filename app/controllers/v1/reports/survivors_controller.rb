class V1::Reports::SurvivorsController < ::V1::ApplicationController
  def index
    values = ::Reports.survivors_by_status
    render json: values.as_json, status: :ok
  end
end
