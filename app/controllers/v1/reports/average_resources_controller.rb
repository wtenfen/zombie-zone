class V1::Reports::AverageResourcesController < ::V1::ApplicationController
  def index
    values = ::Reports.average_resources
    render json: values.as_json, status: :ok
  end
end
