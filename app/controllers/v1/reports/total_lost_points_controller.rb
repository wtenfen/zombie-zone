class V1::Reports::TotalLostPointsController < ::V1::ApplicationController
  def index
    values = ::Reports.total_lost_points
    render json: values.as_json, status: :ok
  end
end
