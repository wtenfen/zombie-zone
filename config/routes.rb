Rails.application.routes.draw do
  namespace :v1, defaults: { :format => :json} do

    resources :survivors, only: [:create]
    resources :items, only: [:index]
    resource  :last_locations, only: [:update]
    resource  :report_infection, only: [:create], controller: :report_infection
    resource  :trade_items, only: [:create]
    namespace  :reports do
      resources :survivors, only: [:index]
      resources :'average_resources', only: [:index]
      resources :'total_lost_points', only: [:index]
    end
  end
end
