require 'rails_helper'

RSpec.describe UpdateToInfected, type: :model do
  describe "call" do
    let!(:survivor) { create(:survivor) }
    let!(:reporter) { create(:survivor) }

    context "when survivor is not infected yet" do
      it "it udpates to infected when 3 warnings are given " do
        expect(survivor.infected?).to be_falsy

        2.times { ContaminationReport.create(reporter: create(:survivor), infected: survivor) }
        described_class.new(reporter, survivor).call
        expect(survivor.reload.infected?).to be_truthy
      end

      it "does not update to infected when less than 3 warnings are given" do
        expect(survivor.infected?).to be_falsy

        described_class.new(reporter, survivor).call
        expect(survivor.reload.infected?).to be_falsy
      end
    end
  end
end
