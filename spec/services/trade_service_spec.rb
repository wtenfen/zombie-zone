require 'rails_helper'

RSpec.describe TradeService, type: :model do
  describe "#call" do
    let(:four_points_item) { create(:item, points: 4)}
    let(:f_trader) { create(:survivor) }
    let(:s_trader) { create(:survivor) }

    context 'when invalid transaction payload provided' do
      before do
        create(:survivor_item, survivor: f_trader, item: four_points_item, total: 2)
        create(:survivor_item, survivor: s_trader, item: four_points_item, total: 2)
      end

      it "should return errors when traders does not have enough amount of items" do
        payload = {:first_trader  => {survivor_id: f_trader.id, :items =>[{item_id: f_trader.items.first.item_id, amount: 3}]},
                   :second_trader => {survivor_id: s_trader.id, :items =>[{item_id: s_trader.items.first.item_id, amount: 3}]}}
        service = described_class.new(payload)
        expect(service.call).to be_falsy
        expect(service.errors.size).to eql(2)
      end
      it "should return errors when amount of points doesnt match" do
        payload = {:first_trader  => {survivor_id: f_trader.id, :items =>[{item_id: f_trader.items.first.item_id, amount: 2}]},
                   :second_trader => {survivor_id: s_trader.id, :items =>[{item_id: s_trader.items.first.item_id, amount: 3}]}}
        service = described_class.new(payload)
        expect(service.call).to be_falsy
        expect(service.errors.size).to eql(1)
      end
    end
  end
end

