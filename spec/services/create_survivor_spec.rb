require 'rails_helper'

RSpec.describe CreateSurvivor, type: :model do
  describe "create" do
  end
end

RSpec.describe CreateSurvivor::NormalizeItemsPayload, type: :model do
  describe "build" do
    let(:payload) { [{id: 1, total: 2},{id: 5, total: nil},{id: 1, total: 2},{id: 5, total: 5}] }

    it "should sums equal items" do
      result = described_class.new(payload).build
      expect(result.size).to be(2)
      expect(result[0][:total]).to be(4)
      expect(result[1][:total]).to be(5)
    end
  end
end
