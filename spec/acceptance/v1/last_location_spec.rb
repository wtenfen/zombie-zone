require 'rails_helper'
require 'rspec_api_documentation/dsl'
require 'acceptance/v1/endpoint_spec_helper'

resource 'Last Location', :endpoint_spec do
  setup_environment
  let!(:survivor) { create(:survivor) }
  let(:raw_post) { params.to_json}

  parameter :location, 'Location object', required: true
  with_options scope: :location do
    parameter :survivor_id, 'Survivor to update location', required: true
    parameter :lat, 'Latitude'
    parameter :long, 'Longitude'
  end

  put '/v1/last_locations' do
    context 'with valid request' do
      let(:location) { { survivor_id: survivor.id, lat: 80, long: 100  } }

      example_request 'Update survivor last location' do
        expect(status).to eql(200)
      end
    end

    context 'with invalid Latitude/longitude' do
      let(:location) { { survivor_id: survivor.id, lat: 'a', long: 'b'  } }

      example 'with invalid attributes', :document => false do
        do_request
        expect(status).to eql(400)
      end
    end
  end
end
