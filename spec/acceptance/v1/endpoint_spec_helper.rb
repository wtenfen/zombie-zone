module V1
  module EndpointSpecHelper
    extend ActiveSupport::Concern

    class_methods do
      def setup_environment &block
        header "Accept", "application/json"
        header "Content-Type", "application/json"
      end
    end

    def json_response
      @json_response ||= JSON.parse(response_body, symbolize_names: true)
    end

  end
end

RSpec.configure do |c|
  c.extend V1::EndpointSpecHelper, :endpoint_spec
  c.include V1::EndpointSpecHelper, :endpoint_spec
end
