require 'rails_helper'
require 'rspec_api_documentation/dsl'
require 'acceptance/v1/endpoint_spec_helper'

resource 'Trade Items', :endpoint_spec do
  setup_environment
  let!(:first_trader)  { create(:survivor) }
  let!(:second_trader) { create(:survivor) }
  let(:raw_post)  { params.to_json}

  parameter :trade, 'Trade object', required: true

  post '/v1/trade_items' do
    let(:four_points_item) { create(:item, points: 4)}

    context 'with valid request' do
      before do
        create(:survivor_item, survivor: first_trader,  item: four_points_item, total: 2)
        create(:survivor_item, survivor: second_trader, item: four_points_item, total: 2)
      end
      let(:trade) { build_raw_trade }

      example_request 'Trade Items' do
        expect(status).to eql(201)
      end

      def build_raw_trade
        {
          first_trader: {
            survivor_id: first_trader.id,
            items: [
              {item_id: four_points_item.id, amount: 1}
            ]
          },
          second_trader: {
            survivor_id: second_trader.id,
            items: [
              {item_id: four_points_item.id, amount: 1}
            ]
          }
        }
      end
    end
  end

end
