require 'rails_helper'
require 'rspec_api_documentation/dsl'
require 'acceptance/v1/endpoint_spec_helper'

resource 'Reports', :endpoint_spec do
  setup_environment
  before do
    create_list(:survivor, 2, status: :active)
    create_list(:survivor, 3, status: :infected)
  end

  get '/v1/reports/survivors' do
    example_request 'Survivors percentage by status' do
      expect(status).to eql(200)
    end
  end
  get '/v1/reports/average_resources' do
    before do
      create_list(:survivor_item, 2)
    end
    example_request 'Average amount of resource by survivor' do
      expect(status).to eql(200)
    end
  end
  get '/v1/reports/total_lost_points' do
    before { create(:survivor_item, survivor: Survivor.infected.first ) }
    example_request 'Total lost points by infection' do
      expect(status).to eql(200)
    end
  end

end
