require 'rails_helper'
require 'rspec_api_documentation/dsl'
require 'acceptance/v1/endpoint_spec_helper'

resource 'Items', :endpoint_spec do
  describe 'Items controller' do
    let!(:items) { create_list(:item, 5) }

    get '/v1/items' do
      setup_environment
      example_request 'List Available Items' do
        expect(status).to eql(200)
        expect(json_response.size).to eql(5)
      end
    end

  end
end
