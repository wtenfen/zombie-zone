require 'rails_helper'
require 'rspec_api_documentation/dsl'
require 'acceptance/v1/endpoint_spec_helper'

resource 'Report infected survivor', :endpoint_spec do
  include ActiveJob::TestHelper
  after do
    clear_enqueued_jobs
  end

  setup_environment

  parameter :infection, 'Survivor object', required: true
  with_options scope: :infection do
    parameter :reporter_id, "Id of reporter of the infection", required: true
    parameter :infected_id, "Id of the infected survivor", required: true
  end

  post '/v1/report_infection' do
    context 'when valid request' do
      let!(:reporter) { create(:survivor) }
      let!(:infected) { create(:survivor) }

      let(:reporter_id) { reporter.id }
      let(:infected_id) { infected.id }

      let(:raw_post) { params.to_json}

      example_request 'Report that a survivor is infected' do
        expect(status).to eql(200)
      end

      example "Should enqueu a ReportedInfectionJob", :document => false do
        expect { do_request }.to have_enqueued_job(ReportedInfectionJob).with(reporter, infected)
      end
    end
  end
end
