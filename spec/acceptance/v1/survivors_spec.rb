require 'rails_helper'
require 'rspec_api_documentation/dsl'
require 'acceptance/v1/endpoint_spec_helper'

resource 'Survivors', :endpoint_spec do
  setup_environment
  let!(:items) { create_list(:item, 5) }

  parameter :survivor, 'Survivor object', required: true
  with_options scope: :survivor do
    parameter :name, "Name", required: true
    parameter :age,  "Age", required: true
    parameter :lat, "Latitude"
    parameter :long, "Longitude"
  end
  with_options scope: :item_attributes do
    parameter :id, "Id of the item", required: true
    parameter :total, "Amount of items"
  end

  post '/v1/survivors' do
    context 'when valid request' do
      let(:survivor) do
        data = attributes_for :survivor
        data[:items_attributes] = [{id: items.first.id, total: 2},{id: items.last.id, total: 1}, {id: items.first.id, total: 2},{id: items.last.id, total: 5}]
        data
      end
      let(:raw_post) { params.to_json}

      example_request 'Add new survivor' do
        expect(status).to eql(201)
      end
    end

    context 'when missing parameters' do
      let(:survivor) { attributes_for :survivor, name: nil }
      let(:raw_post) { params.to_json}

      example 'missing parameter', :document => false do
        do_request
        expect(status).to eql(400)
      end
    end
  end
end
