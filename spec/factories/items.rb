FactoryBot.define do
  factory :item do
    title { Faker::Commerce.material }
    points { Random.new.rand(1..4)}
  end
end
