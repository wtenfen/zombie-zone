FactoryBot.define do
  factory :survivor do
    name { Faker::Name.name }
    age { Random.new.rand(1..100) }
    status 'active'
    lat { Random.new.rand(-90.000000000...90.000000000) }
    long { Random.new.rand(-180.000000000...180.000000000) }
    gender { ["male", "female"].sample }
  end

  factory :survivor_item do
    survivor { create(:survivor) }
    item { create(:item) }
    total { Random.new.rand(1..5) }
  end
end
