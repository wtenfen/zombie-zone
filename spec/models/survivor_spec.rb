# == Schema Information
#
# Table name: survivors
#
#  id         :bigint(8)        not null, primary key
#  name       :string
#  age        :integer
#  gender     :string(25)
#  lat        :float
#  long       :float
#  status     :string(25)       default("active")
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Survivor, type: :model do
  describe 'factories' do
    it 'has a valid factory' do
      expect(build(:survivor)).to be_valid
    end
  end
end
