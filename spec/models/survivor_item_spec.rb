# == Schema Information
#
# Table name: survivor_items
#
#  id          :bigint(8)        not null, primary key
#  survivor_id :bigint(8)
#  item_id     :bigint(8)
#  total       :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'rails_helper'

RSpec.describe SurvivorItem, type: :model do
  describe 'factories' do
    it 'has a valid factory' do
      expect(build(:survivor_item)).to be_valid
    end
  end
end
