# == Schema Information
#
# Table name: items
#
#  id         :bigint(8)        not null, primary key
#  title      :string
#  points     :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Item, type: :model do
  describe 'factories' do
    it 'has a valid factory' do
      expect(build(:item)).to be_valid
    end
  end
end
