require 'rails_helper'

RSpec.describe 'Rescue API exceptions', type: :controller  do
  controller(MainController) do
    def index
      raise ActionController::ParameterMissing, :test
    end
    def create
      raise ActiveRecord::RecordNotFound, :test
    end
  end

  it "rescue parameter missing with 422" do
    get :index
    expect(response).to have_http_status(:unprocessable_entity)
  end
  it "rescue ActiveRecord RecordNotFound with 400" do
    post :create
    expect(response).to have_http_status(:bad_request)
  end
end
