puts '== Creating Items =='
items = Item.create([{title: 'Water', points: 4}, {title: 'Food', points: 3}, {title: 'Medication', points: 2},
                     {title: 'Ammunition', points: 1}])

if Rails.env.development?
  puts '== Creating Survivor =='
  survivor = Survivor.find_or_initialize_by(name: 'Joe Doe', age: 12, gender: 'male', lat: 35.929673, long: -78.948237)
  unless survivor.persisted?
    survivor.items << SurvivorItem.new(item: Item.first, total: 5)
  end
  survivor.save
end
