class CreateSurvivors < ActiveRecord::Migration[5.2]
  def change
    create_table :survivors do |t|
      t.string    :name
      t.integer   :age
      t.string    :gender, limit: 25
      t.float     :lat
      t.float     :long
      t.string    :status, default: 'active', limit: 25

      t.timestamps
    end
  end
end
