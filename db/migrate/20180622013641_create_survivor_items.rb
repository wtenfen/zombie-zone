class CreateSurvivorItems < ActiveRecord::Migration[5.2]
  def change
    create_table :survivor_items do |t|
      t.references :survivor, foreign_key: true
      t.references :item, foreign_key: true
      t.integer :total, default: 0

      t.timestamps
    end
    add_index :survivor_items, [:survivor_id, :item_id], unique: true
  end
end
