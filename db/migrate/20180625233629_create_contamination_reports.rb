class CreateContaminationReports < ActiveRecord::Migration[5.2]
  def change
    create_table :contamination_reports do |t|
      t.references :reporter, foreign_key: {to_table: :survivors, name: :reporter_id_on_contamination_reports}
      t.references :infected, foreign_key: {to_table: :survivors, name: :infected_id_on_contamination_reports}
      t.timestamps
    end
    add_index :contamination_reports, [:reporter_id, :infected_id], unique: true
  end
end
