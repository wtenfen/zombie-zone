# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_06_25_233629) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "contamination_reports", force: :cascade do |t|
    t.bigint "reporter_id"
    t.bigint "infected_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["infected_id"], name: "index_contamination_reports_on_infected_id"
    t.index ["reporter_id", "infected_id"], name: "index_contamination_reports_on_reporter_id_and_infected_id", unique: true
    t.index ["reporter_id"], name: "index_contamination_reports_on_reporter_id"
  end

  create_table "items", force: :cascade do |t|
    t.string "title"
    t.integer "points"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "survivor_items", force: :cascade do |t|
    t.bigint "survivor_id"
    t.bigint "item_id"
    t.integer "total", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["item_id"], name: "index_survivor_items_on_item_id"
    t.index ["survivor_id", "item_id"], name: "index_survivor_items_on_survivor_id_and_item_id", unique: true
    t.index ["survivor_id"], name: "index_survivor_items_on_survivor_id"
  end

  create_table "survivors", force: :cascade do |t|
    t.string "name"
    t.integer "age"
    t.string "gender", limit: 25
    t.float "lat"
    t.float "long"
    t.string "status", limit: 25, default: "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "contamination_reports", "survivors", column: "infected_id", name: "infected_id_on_contamination_reports"
  add_foreign_key "contamination_reports", "survivors", column: "reporter_id", name: "reporter_id_on_contamination_reports"
  add_foreign_key "survivor_items", "items"
  add_foreign_key "survivor_items", "survivors"
end
